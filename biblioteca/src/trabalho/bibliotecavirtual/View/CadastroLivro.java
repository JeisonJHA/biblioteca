package trabalho.bibliotecavirtual.View;

import trabalho.bibliotecavirtual.Model.BusinessObject.FuncoesBasicas;
import trabalho.bibliotecavirtual.Model.BusinessObject.RegrasLivro;
import trabalho.bibliotecavirtual.Model.ValueObject.Livro;

public class CadastroLivro extends Cadastro {

	@Override
	public void novo() throws Exception {
		String titulo = FuncoesBasicas
				.informarCampoString("Informe o titulo do livro:");
		String autor = FuncoesBasicas
				.informarCampoString("Informe o nome do autor:");
		RegrasLivro regras = new RegrasLivro();

		Livro livro = new Livro(titulo, autor);
		regras.inserirLivro(livro);

	}

	@Override
	public void excluir() throws Exception {
		String titulo = FuncoesBasicas
				.informarCampoString("Informe o titulo do livro:");
		String autor = FuncoesBasicas
				.informarCampoString("Informe o nome do autor:");
		RegrasLivro regras = new RegrasLivro();

		Livro livro = new Livro(titulo, autor);
		regras.removerLivro(livro);
	}

}
