package trabalho.bibliotecavirtual.View;

import javax.swing.JOptionPane;

import trabalho.bibliotecavirtual.Model.BusinessObject.CancelaOperacao;
import trabalho.bibliotecavirtual.Model.BusinessObject.FuncoesBasicas;
import trabalho.bibliotecavirtual.Model.BusinessObject.RegrasEmprestimo;
import trabalho.bibliotecavirtual.Model.BusinessObject.ValidacaoExemplar;
import trabalho.bibliotecavirtual.Model.ValueObject.Emprestimo;

public class TelaEmprestimo {

	public TelaEmprestimo() {
		boolean continua = true;
		int opcao = 0;
		Emprestimo emprestimo = new Emprestimo();
		RegrasEmprestimo regras = null;

		regras = new RegrasEmprestimo();
		while (continua) {
			try {
				emprestimo.limpar();
				opcao = Menu();
				switch (opcao) {
				case 1:
					preencherDados(emprestimo);
					regras.emprestarExemplar(emprestimo);
					break;
				case 2:
					preencherDados(emprestimo);
					regras.devolverExemplar(emprestimo);
					break;
				case 3:
					continua = false;
					break;
				default:
					JOptionPane.showMessageDialog(null, "Valor inv�lido.");
					break;
				}
			} catch (NumberFormatException e) {
				continua = false;
				break;
			} catch (CancelaOperacao e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			} catch (ValidacaoExemplar e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getClass());
			}
		}
	}

	private void preencherDados(Emprestimo emprestimo) throws CancelaOperacao {
		emprestimo.setMatricula(FuncoesBasicas
				.informarCampoInt("Informe a matricula do cliente."));
		emprestimo.setTitulo(FuncoesBasicas
				.informarCampoString("Informe o titulo do livro:"));
		emprestimo.setAutor(FuncoesBasicas
				.informarCampoString("Informe o autor:"));
		emprestimo.setEdicao(FuncoesBasicas
				.informarCampoString("Informe a edicao:"));
	}

	private int Menu() {
		return Integer.parseInt(JOptionPane
				.showInputDialog("Selecione a op��o: \n"
						+ "1 - Emprestar exemplar. \n"
						+ "2 - Devolver exemplar. \n" + "3 - Sair."));
	}

	public static void main(String[] args) {
		new TelaEmprestimo();

	}

}
