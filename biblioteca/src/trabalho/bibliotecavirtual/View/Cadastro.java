package trabalho.bibliotecavirtual.View;

public abstract class Cadastro {
	public abstract void novo() throws Exception;
	public abstract void excluir() throws Exception;
}
