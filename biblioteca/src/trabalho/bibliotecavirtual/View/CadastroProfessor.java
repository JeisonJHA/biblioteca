package trabalho.bibliotecavirtual.View;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import trabalho.bibliotecavirtual.Model.BusinessObject.FuncoesBasicas;
import trabalho.bibliotecavirtual.Model.BusinessObject.RegrasCliente;
import trabalho.bibliotecavirtual.Model.ValueObject.Professor;

public class CadastroProfessor extends CadastroPessoa {
	protected List<String> listacurso;
	
	@Override
	public void novo() throws Exception {
		super.novo();
		listacurso = new ArrayList<String>();
		String curso = null;
		boolean continua = true;
		while (continua) {
			curso = FuncoesBasicas
				.informarCampoString("Informe o curso em que o professor leciona aula:");
			
			listacurso.add(curso);
			continua = JOptionPane.showConfirmDialog(null,
					"Deseja informar mais algum curso?", "Sim ou n�o?",
					JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
		}

		Professor professor = new Professor(matricula, nome, listacurso);
		RegrasCliente regras = new RegrasCliente();		
		regras.inserirCliente(professor);
		
	}
}
