package trabalho.bibliotecavirtual.View;

import trabalho.bibliotecavirtual.Model.BusinessObject.FuncoesBasicas;
import trabalho.bibliotecavirtual.Model.BusinessObject.RegrasExemplar;
import trabalho.bibliotecavirtual.Model.ValueObject.Exemplar;

public class CadastroExemplar extends Cadastro {

	@Override
	public void novo() throws Exception {
		String titulo = FuncoesBasicas
				.informarCampoString("Informe o titulo do livro:");
		RegrasExemplar regras = new RegrasExemplar();
		Exemplar exemplar = preencherDados();
		regras.inserirExemplar(titulo, exemplar);
	}

	@Override
	public void excluir() throws Exception {
		String titulo = FuncoesBasicas
				.informarCampoString("Informe o titulo do livro:");
		RegrasExemplar regras = new RegrasExemplar();
		Exemplar exemplar = preencherDados();
		regras.removerExemplar(titulo, exemplar);
	}

	private Exemplar preencherDados() throws Exception {
		String localizacao = FuncoesBasicas
				.informarCampoString("Informe a localiza��o do exemplar:");
		String edicao = FuncoesBasicas
				.informarCampoString("Informe a edi��o do exemplar:");
		int quantidade = FuncoesBasicas
				.informarCampoInt("Informe a quantidade do exemplar:");

		return new Exemplar(localizacao, edicao, quantidade);
	}
}
