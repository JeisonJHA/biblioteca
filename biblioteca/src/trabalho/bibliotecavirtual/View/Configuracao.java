package trabalho.bibliotecavirtual.View;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import trabalho.bibliotecavirtual.Model.BusinessObject.FuncoesBasicas;

public final class Configuracao extends Properties {
	private static final long serialVersionUID = 2233762198075071575L;
	
	public Configuracao() {
		try {
			this.load(new FileInputStream("./dados/dados.properties"));
			FuncoesBasicas.TIPO = Integer.parseInt(this.getProperty("tipopersistencia"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
