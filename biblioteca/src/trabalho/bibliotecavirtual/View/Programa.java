package trabalho.bibliotecavirtual.View;

import javax.swing.JOptionPane;

import trabalho.bibliotecavirtual.Model.BusinessObject.CancelaOperacao;
import trabalho.bibliotecavirtual.Model.BusinessObject.PersistenciaInvalida;

public class Programa {
	public Programa() {
		new Configuracao();
		boolean continua = true;
		int opcao = 0;
		Cadastro cadastro;

		while (continua) {
			try {

				opcao = Menu();
				switch (opcao) {
				case 1:
					cadastro = getTipoCadastroPessoa("Deseja cadastrar aluno?");
					cadastro.novo();
					break;
				case 2:
					cadastro = getTipoCadastroPessoa("Deseja excluir aluno?");
					cadastro.excluir();
					break;
				case 3:
					cadastro = new CadastroLivro();
					cadastro.novo();
					break;
				case 4:
					cadastro = new CadastroLivro();
					cadastro.excluir();
					break;
				case 5:
					cadastro = new CadastroExemplar();
					cadastro.novo();
					break;
				case 6:
					cadastro = new CadastroExemplar();
					cadastro.excluir();
					break;
				case 7:
					@SuppressWarnings("unused")
					TelaEmprestimo tela = new TelaEmprestimo();
					break;
				case 8:
					@SuppressWarnings("unused")
					Consulta consulta = new Consulta();
					break;
				case 9:
					cadastro = new CadastroCurso();
					cadastro.novo();
					break;
				case 10:
					cadastro = new CadastroCurso();
					cadastro.excluir();
					break;
				case 11:
					continua = false;
					break;
				default:
					JOptionPane.showMessageDialog(null, "Valor inv�lido.");
					continua = false;
					break;
				}
			} catch (NumberFormatException e) {
				continua = false;
				break;
			} catch (CancelaOperacao | PersistenciaInvalida e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		}
	}

	private CadastroPessoa getTipoCadastroPessoa(String msg)
			throws CancelaOperacao {
		int sele = JOptionPane.showConfirmDialog(null, msg, "Sim ou n�o?",
				JOptionPane.YES_NO_OPTION);
		if (sele == JOptionPane.YES_OPTION) {
			return new CadastroAluno();
		} else if (sele == JOptionPane.NO_OPTION) {
			return new CadastroProfessor();
		} else {
			throw new CancelaOperacao();
		}
	}

	private int Menu() {
		return Integer.parseInt(JOptionPane
				.showInputDialog("Selecione a op��o: \n"
						+ "1 - Inserir cliente. \n" + "2 - Excluir cliente. \n"
						+ "3 - Inserir livro. \n" + "4 - Excluir livro. \n"
						+ "5 - Inserir exemplar. \n"
						+ "6 - Remover exemplar. \n"
						+ "7 - Emprestimo exemplar. \n" + "8 - Relat�rios. \n"
						+ "9 - Inserir curso \n" + "10 - Remover curso \n"
						+ "11 - Sair."));
	}

	public static void main(String[] args) {
		new Programa();
	}
}
