package trabalho.bibliotecavirtual.View;

import trabalho.bibliotecavirtual.Model.BusinessObject.FuncoesBasicas;
import trabalho.bibliotecavirtual.Model.BusinessObject.RegrasCliente;
import trabalho.bibliotecavirtual.Model.ValueObject.Aluno;

public class CadastroAluno extends CadastroPessoa {
	protected String curso;
	
	@Override
	public void novo() throws Exception {
		super.novo();
		curso = FuncoesBasicas.informarCampoString("Informe o curso em que o aluno est� matriculado:");
		RegrasCliente regras = new RegrasCliente();
		Aluno aluno = new Aluno(matricula, nome, curso);
		regras.inserirCliente(aluno);
		
	}

}
