package trabalho.bibliotecavirtual.View;

import javax.swing.JOptionPane;

import trabalho.bibliotecavirtual.Model.BusinessObject.FuncoesBasicas;
import trabalho.bibliotecavirtual.Model.BusinessObject.RegrasCliente;
import trabalho.bibliotecavirtual.Model.BusinessObject.RegrasCurso;
import trabalho.bibliotecavirtual.Model.BusinessObject.RegrasEmprestimo;
import trabalho.bibliotecavirtual.Model.BusinessObject.RegrasExemplar;
import trabalho.bibliotecavirtual.Model.BusinessObject.RegrasLivro;
import trabalho.bibliotecavirtual.Model.ValueObject.Emprestimo;

public class Consulta {
	public Consulta() {
		boolean continua = true;
		int opcao = 0;
		Emprestimo emprestimo = new Emprestimo();
		while (continua) {
			try {
				emprestimo.limpar();
				opcao = Menu();
				switch (opcao) {
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
					buscaValores(opcao);
					break;
				case 7:
					continua = false;
					break;
				default:
					JOptionPane.showMessageDialog(null, "Valor inv�lido.");
					break;
				}

			} catch (NumberFormatException e) {
				continua = false;
				break;
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		}
	}

	private void buscaValores(int Valor) throws Exception {
		String consulta = "";
		RegrasCliente regrasCliente;
		RegrasCurso regrasCurso;
		RegrasLivro regrasLivro;
		RegrasExemplar regrasExemplar;
		RegrasEmprestimo regrasEmprestimo;
		switch (Valor) {
		case 1:
			try {
				regrasCliente = new RegrasCliente();
				consulta = regrasCliente
						.consultarClientes(FuncoesBasicas.ALUNO);
			} finally {
				regrasCliente = null;
			}
			break;
		case 2:
			try {
				regrasCliente = new RegrasCliente();
				consulta = regrasCliente
						.consultarClientes(FuncoesBasicas.PROFESSOR);
			} finally {
				regrasCliente = null;
			}
			break;
		case 3:
			try {
				regrasCurso = new RegrasCurso();
				consulta = regrasCurso.consultarCursos();
			} finally {
				regrasCurso = null;
			}
			break;
		case 4:
			try {
				regrasLivro = new RegrasLivro();
				consulta = regrasLivro.consultarLivros();
			} finally {
				regrasLivro = null;
			}
			break;
		case 5:
			try {
				regrasExemplar = new RegrasExemplar();
				consulta = regrasExemplar.consultarExemplares();
			} finally {
				regrasExemplar = null;
			}
			break;		
		case 6:
			try {
				regrasEmprestimo = new RegrasEmprestimo();
				consulta = regrasEmprestimo.consultarEmprestimos();
			} finally {
				regrasExemplar = null;
			}
			break;
		}
		if (consulta == null || consulta.isEmpty()) {
			JOptionPane.showMessageDialog(null,
					"Consulta n�o trouxe resultado.");
			return;
		}
		JOptionPane.showMessageDialog(null, consulta);
	}

	private int Menu() {
		return Integer.parseInt(JOptionPane
				.showInputDialog("Selecione a op��o: \n"
						+ "1 - Listar alunos. \n"
						+ "2 - Listar professores. \n"
						+ "3 - Listar cursos. \n" 
						+ "4 - Listar livros. \n"
						+ "5 - Listar exemplares. \n"
						+ "6 - Listar emprestimos. \n" 
						+ "7 - Sair."));
	}

	public static void main(String[] args) {
		new Consulta();

	}

}
