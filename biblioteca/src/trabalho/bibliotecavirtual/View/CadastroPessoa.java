package trabalho.bibliotecavirtual.View;

import javax.swing.JOptionPane;

import trabalho.bibliotecavirtual.Model.BusinessObject.CancelaOperacao;
import trabalho.bibliotecavirtual.Model.BusinessObject.FuncoesBasicas;
import trabalho.bibliotecavirtual.Model.BusinessObject.RegrasCliente;

public abstract class CadastroPessoa extends Cadastro {
	protected int matricula;
	protected String nome;

	@Override
	public void novo() throws Exception {
		// IPessoaDAO banco;
		// banco = DAOFactory.getDAOFactory(1).getPessoaDAO();
		// matricula = banco.pegarProximaMatricula();
		matricula = FuncoesBasicas.informarCampoInt("Informe a matricula:");
		nome = FuncoesBasicas.informarCampoString("Informe o nome:");
	}

	@Override
	public void excluir() throws Exception {
		try {
			nome = FuncoesBasicas.informarCampoString("Informe o nome:");
			RegrasCliente regras = new RegrasCliente();
			regras.excluirCliente(nome);
		} catch (CancelaOperacao e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}
}
