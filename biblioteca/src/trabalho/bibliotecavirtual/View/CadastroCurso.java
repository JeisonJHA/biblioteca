package trabalho.bibliotecavirtual.View;

import trabalho.bibliotecavirtual.Model.BusinessObject.FuncoesBasicas;
import trabalho.bibliotecavirtual.Model.BusinessObject.RegrasCurso;

public class CadastroCurso extends Cadastro {
	@Override
	public void novo() throws Exception {
		String nomecurso = FuncoesBasicas
				.informarCampoString("Digite o nome do curso");
		RegrasCurso regras = new RegrasCurso();
		regras.cadastrarCurso(nomecurso);
	}

	@Override
	public void excluir() throws Exception {
		String nomecurso = FuncoesBasicas
				.informarCampoString("Digite o nome do curso");
		RegrasCurso regras = new RegrasCurso();
		regras.removerCurso(nomecurso);
	}
}
