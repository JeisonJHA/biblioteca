package trabalho.bibliotecavirtual.Model.ValueObject;

public class Aluno extends Pessoa {
	private static final long serialVersionUID = -2682751088296381593L;
	private String CursoMatriculado;

	public Aluno() {}
	
	public Aluno(int matricula, String nome, String curso) {
		setMatricula(matricula);
		setNome(nome);
		setCursoMatriculado(curso);
	}

	public String getCursoMatriculado() {
		return CursoMatriculado;
	}

	public void setCursoMatriculado(String cursoMatriculado) {
		CursoMatriculado = cursoMatriculado;
	}

	public String toString(){
		return super.toString() + "; Curso: " + CursoMatriculado;		
	}
}
