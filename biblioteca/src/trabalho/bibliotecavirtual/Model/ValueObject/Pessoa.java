package trabalho.bibliotecavirtual.Model.ValueObject;

import java.io.Serializable;

public abstract class Pessoa implements Serializable{
	private static final long serialVersionUID = 568504670974143611L;
	private int Codigo;
	private int Matricula;
	private String Nome;
	
	public int getMatricula() {
		return Matricula;
	}
	public void setMatricula(int matricula) {
		Matricula = matricula;
	}
	public String getNome() {
		return Nome;
	}
	public void setNome(String nome) {
		Nome = nome;
	}
	public String toString(){
		return "Matricula: " + Matricula + "; Nome: " + Nome;		
	}
	public int getCodigo() {
		return Codigo;
	}
	public void setCodigo(int codigo) {
		Codigo = codigo;
	}

}
