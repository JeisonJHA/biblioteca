package trabalho.bibliotecavirtual.Model.ValueObject;

import java.util.List;

public class Professor extends Pessoa {
	private static final long serialVersionUID = -7895867953519557036L;
	private List<String> Cursos;
	
	public Professor() {}
	
	public Professor(int matricula, String nome, List<String>curso) {
		setMatricula(matricula);
		setNome(nome);		
		Cursos = curso;
	}

	public List<String> getCursos() {
		return Cursos;
	}

	public void setCurso(String curso) {
		Cursos.add(curso);
	}

	public String toString(){
		return super.toString() + "; Cursos: " + Cursos();		
	}
	
	private String Cursos(){
		String result = "";
		for (int i = 0; i < Cursos.size()-1; i++) {
			String curso = Cursos.get(i);
			result =+ (i + 1) + " " + curso + " ";			
		}
		return result;
	}
}
