package trabalho.bibliotecavirtual.Model.ValueObject;

import java.io.Serializable;

public class Exemplar implements Serializable {
	private static final long serialVersionUID = -7547668598573952877L;
	private int Codigo;
	private String Localizacao;
	private String Edicao;
	private int Quantidade;

	public Exemplar() {}

	public Exemplar(String localizacao, String edicao, int quantidade)
			throws Exception {
		setLocalizacao(localizacao);
		setEdicao(edicao);
		setQuantidade(quantidade);
	}

	public int getCodigo() {
		return Codigo;
	}

	public void setCodigo(int codigo) {
		Codigo = codigo;
	}

	public String getLocalizacao() {
		return Localizacao;
	}

	public void setLocalizacao(String localizacao) {
		Localizacao = localizacao;
	}

	public String getEdicao() {
		return Edicao;
	}

	public void setEdicao(String edicao) {
		Edicao = edicao;
	}

	public int getQuantidade() {
		return Quantidade;
	}

	public void setQuantidade(int quantidade) {
		Quantidade = quantidade;
	}

	public String toString() {
		return "Codigo: " + Codigo + "; Localiza��o: " + Localizacao
				+ "; Edi��o: " + Edicao + "; Quantidade: " + Quantidade;
	}

}
