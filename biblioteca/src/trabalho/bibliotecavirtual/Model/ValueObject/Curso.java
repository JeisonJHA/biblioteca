package trabalho.bibliotecavirtual.Model.ValueObject;

import java.io.Serializable;

public class Curso implements Serializable{
	private static final long serialVersionUID = 2358875558790500285L;
	private int Codigo;
	private String Nome;

	public Curso() {		
	}
	
	public Curso(String nome) {
		setNome(nome);
	}

	public String getNome() {
		return Nome;
	}

	public void setNome(String nome) {
		Nome = nome;
	}
	
	public String toString(){
		return "Nome: " + Nome;
	}

	public int getCodigo() {
		return Codigo;
	}

	public void setCodigo(int codigo) {
		Codigo = codigo;
	}

}
