package trabalho.bibliotecavirtual.Model.ValueObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import trabalho.bibliotecavirtual.Model.BusinessObject.CancelaOperacao;

public class Livro implements Serializable{
	private static final long serialVersionUID = -4270468911281567686L;
	private int Codigo;
	private String Titulo;
	private String Autor;
	private List<Exemplar> Exemplares = new ArrayList<Exemplar>();

	public Livro() {}
	
	public Livro(String titulo, String autor) throws Exception {
		setTitulo(titulo);
		setAutor(autor);
	}

	public String getTitulo() {
		return Titulo;
	}

	public void setTitulo(String titulo) {
		Titulo = titulo;
	}

	public String getAutor() {
		return Autor;
	}

	public void setAutor(String autor) {
		Autor = autor;
	}

	public List<Exemplar> getExemplares() {
		return Exemplares;
	}

	public void setExemplares(List<Exemplar> exemplares) {
		Exemplares = exemplares;
	}
	
	public void adicionaExemplar(Exemplar exemplar) {
		if (Exemplares == null)
			Exemplares = new ArrayList<Exemplar>();
		Exemplares.add(exemplar);
	}

	public int getCodigo() {
		return Codigo;
	}

	public void setCodigo(int codigo) {
		Codigo = codigo;
	}
	
	public void removeExemplar(Exemplar exemplar) throws CancelaOperacao{
		if (!Exemplares.remove(exemplar))
			throw new CancelaOperacao("Exemplar inexistente.");
	}
	
	public String toString(){
		return "Codigo: " + Codigo + "; Titulo: " + Titulo + "; Autor: " + Autor + "; Exemplares: \n" + Exemplares();
	}
	
	private String Exemplares(){
		String result = "";
		for (Exemplar exemplar : Exemplares) {
			result += "    " + exemplar.toString() + "\n";
		}
		return result;
	}
}
