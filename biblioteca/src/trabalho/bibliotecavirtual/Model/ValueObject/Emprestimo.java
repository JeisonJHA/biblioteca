package trabalho.bibliotecavirtual.Model.ValueObject;

import java.io.Serializable;
import java.util.Date;

public class Emprestimo implements Serializable {
	private static final long serialVersionUID = 6367996592232254895L;
	private int Codigo;
	private int Matricula;
	private String Titulo;
	private String Autor;
	private String Edicao;
	private int CodigoExemplar;
	private int CodigoLivro;
	private Date DataInicio;
	private Date DataFim;
	private Date DataEntrega;

	public Emprestimo() {
	}

	public Emprestimo(int matricula, String titulo, String autor,
			String edicao, Date dataInicio, Date dataFim) throws Exception {		
		setMatricula(matricula);
		setTitulo(titulo);
		setAutor(autor);
		setEdicao(edicao);
		setDataInicio(dataInicio);
		setDataFim(dataFim);
	}

	public Date getDataInicio() {
		return DataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		DataInicio = dataInicio;
	}

	public Date getDataFim() {
		return DataFim;
	}

	public void setDataFim(Date dataFim) {
		DataFim = dataFim;
	}

	public Date getDataEntrega() {
		return DataEntrega;
	}

	public void setDataEntrega(Date dataEntrega) {
		DataEntrega = dataEntrega;
	}

	public int getMatricula() {
		return Matricula;
	}

	public void setMatricula(int matricula) {
		Matricula = matricula;
	}

	public String getTitulo() {
		return Titulo;
	}

	public void setTitulo(String titulo) {
		Titulo = titulo;
	}

	public String getAutor() {
		return Autor;
	}

	public void setAutor(String autor) {
		Autor = autor;
	}

	public String getEdicao() {
		return Edicao;
	}

	public void setEdicao(String edicao) {
		Edicao = edicao;
	}

	public void limpar() {
		Matricula = -1;
		Titulo = null;
		Autor = null;
		Edicao = null;
	}

	public int getCodigo() {
		return Codigo;
	}

	public void setCodigo(int codigo) {
		Codigo = codigo;
	}

	public int getCodigoLivro() {
		return CodigoLivro;
	}

	public void setCodigoLivro(int codigoLivro) {
		CodigoLivro = codigoLivro;
	}

	public int getCodigoExemplar() {
		return CodigoExemplar;
	}

	public void setCodigoExemplar(int codigoExemplar) {
		CodigoExemplar = codigoExemplar;
	}
	
	public String toString(){
		return "Codigo: " + Codigo +
		"; Matricula: " + Matricula +
		"; T�tulo: " + Titulo +
		"; Autor: " + Autor +
		"; Edi��o: " + Edicao +
		"; Data do in�cio: " + DataInicio +
		"; Data do fim: " + DataFim +
		"; Data da entrega: " + DataEntrega + "\n";
		
	}
}
