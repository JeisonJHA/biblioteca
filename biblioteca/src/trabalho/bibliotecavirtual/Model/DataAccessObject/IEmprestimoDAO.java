package trabalho.bibliotecavirtual.Model.DataAccessObject;

import trabalho.bibliotecavirtual.Model.ValueObject.Emprestimo;

// Interface that all EmprestimoDAOs must support
public interface IEmprestimoDAO {
	public void emprestarExemplar(Emprestimo emprestimo) throws Exception;
	public void devolverExemplar(Emprestimo emprestimo) throws Exception;
	public int consultarQuantidadeExemplar(Emprestimo emprestimo) throws Exception;
	public Emprestimo consultarEmprestimo(Emprestimo emprestimo);
	public String consultarEmprestimos();
}