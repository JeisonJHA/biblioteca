package trabalho.bibliotecavirtual.Model.DataAccessObject;

// Cloudscape concrete DAO Factory implementation

public class ArquivoDAOFactory extends DAOFactory {

	public ICursoDAO getCursoDAO() {
		// ArquivoCursoDAO implements CursoDAO
		return new ArquivoCursoDAO();
	}

	public ILivroDAO getLivroDAO() {
		// ArquivoLivroDAO implements LivroDAO
		return new ArquivoLivroDAO();
	}

	public IPessoaDAO getPessoaDAO() {
		// ArquivoPessoaDAO implements PessoaDAO
		return new ArquivoPessoaDAO();
	}

	public IExemplarDAO getExemplarDAO() {
		// ArquivoExemplarDAO implements ExemplarDAO
		return new ArquivoExemplarDAO();
	}

	public IEmprestimoDAO getEmprestimoDAO() {
		// ArquivoEmprestimoDAO implements EmprestimoDAO
		return new ArquivoEmprestimoDAO();
	}
}