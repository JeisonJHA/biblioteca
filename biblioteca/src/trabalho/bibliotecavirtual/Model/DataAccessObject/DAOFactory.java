package trabalho.bibliotecavirtual.Model.DataAccessObject;

import trabalho.bibliotecavirtual.Model.BusinessObject.FuncoesBasicas;

// Abstract class DAO Factory
public abstract class DAOFactory {

	// List of DAO types supported by the factory
	public static final int ARQUIVO = 1;
	public static final int POSTGRESQL = 2;

	// There will be a method for each DAO that can be
	// created. The concrete factories will have to
	// implement these methods.
	public abstract ICursoDAO getCursoDAO();
	public abstract ILivroDAO getLivroDAO();
	public abstract IPessoaDAO getPessoaDAO();
	public abstract IExemplarDAO getExemplarDAO();
	public abstract IEmprestimoDAO getEmprestimoDAO();

	public static DAOFactory getDAOFactory() {

		switch (FuncoesBasicas.TIPO) {
		case ARQUIVO:
			return new ArquivoDAOFactory();
		case POSTGRESQL:
			return new PostgreDAOFactory();
		default:
			return null;
		}
	}
}