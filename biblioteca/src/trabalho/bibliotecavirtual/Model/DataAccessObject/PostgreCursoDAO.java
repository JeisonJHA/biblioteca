package trabalho.bibliotecavirtual.Model.DataAccessObject;

// PostgreCursoDAO implementation of the 
// CursoDAO interface. This class can contain all
// Postgre specific code and SQL statements. 
// The client is thus shielded from knowing 
// these implementation details.

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import trabalho.bibliotecavirtual.Model.BusinessObject.ErroBanco;
import trabalho.bibliotecavirtual.Model.ValueObject.Curso;

import com.sun.rowset.CachedRowSetImpl;

public class PostgreCursoDAO extends DBBase implements ICursoDAO {

	public PostgreCursoDAO() {
		// initialization
	}

	public void inserirCurso(String nome) throws ErroBanco {
		List<Parametro> parametros = new ArrayList<Parametro>();
		parametros.add(new Parametro(nome, TipoParametro.Texto));
		executaIntervencao("INSERT INTO Curso(nome) VALUES(?)", parametros);
	}

	public void removerCurso(String nome) throws ErroBanco {
		List<Parametro> parametros = new ArrayList<Parametro>();
		parametros.add(new Parametro(nome, TipoParametro.Texto));
		executaIntervencao("DELETE FROM Curso WHERE nome = ?", parametros);
	}

	public Curso consultarCurso(String nome) throws ErroBanco {
		CachedRowSetImpl resultado = null;
		try {
			List<Parametro> parametros = new ArrayList<Parametro>();
			parametros.add(new Parametro(nome, TipoParametro.Texto));
			resultado = executaConsulta(
					"SELECT id_curso, nome FROM Curso WHERE nome = ?)",
					parametros);
			return new Curso(resultado.getString(2));
		} catch (SQLException e) {
			throw new ErroBanco("Erro na leitura de dados. Mensagem original: "
					+ e.getMessage());
		}
	}

	public String consultarCursos() throws ErroBanco {
		CachedRowSetImpl resultado = null;
		resultado = executaConsulta("SELECT nome FROM Curso");
		String result = "";
		try {
			while (resultado.next())
				result += "Curso: " + resultado.getString(1) + "\n";
		} catch (SQLException e) {
			throw new ErroBanco("Erro na leitura de dados. Mensagem original: "
					+ e.getMessage());
		}
		return result;
	}

	public void inserirCurso(Curso curso) throws ErroBanco {
		inserirCurso(curso.getNome());
	}
}