package trabalho.bibliotecavirtual.Model.DataAccessObject;

import trabalho.bibliotecavirtual.Model.ValueObject.Pessoa;

// Interface that all PessoaDAOs must support
public interface IPessoaDAO {
	public void inserirCliente(Pessoa pessoa) throws Exception;
	public void removerCliente(Pessoa pessoa) throws Exception;
	public Pessoa consultarCliente(String nome) throws Exception;
	public Pessoa consultarCliente(int matricula) throws Exception;
	public String consultarClientes(int tipoCliente) throws Exception;
	public void excluirCliente(String nome) throws Exception;
}