package trabalho.bibliotecavirtual.Model.DataAccessObject;

// ArquivoLivroDAO implementation of the 
// LivroDAO interface. This class can contain all
// Arquivo specific code and SQL statements. 
// The client is thus shielded from knowing 
// these implementation details.

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import trabalho.bibliotecavirtual.Model.BusinessObject.ErroBanco;
import trabalho.bibliotecavirtual.Model.ValueObject.Livro;

public class ArquivoLivroDAO extends ArquivoBase implements ILivroDAO {
	private static List<Livro> Livros;

	public ArquivoLivroDAO() {
		Livros = new ArrayList<Livro>();
		deserializarLivro();
	}

	@SuppressWarnings("unchecked")
	private void deserializarLivro() {
		try {
			Livros = (List<Livro>) deserializarObjeto("Livro");
		} catch (IOException e) {
		}
	}

	private void serializarLivro(List<Livro> livro) throws IOException {
		serializarObjeto(livro, "Livro");
	}

	private static int pegarProximoCodigo() {
		if (Livros.isEmpty())
			return 1;

		return Livros.get(Livros.lastIndexOf(Livros)).getCodigo();
	}

	public void inserirLivro(Livro livro) throws IOException {
		livro.setCodigo(pegarProximoCodigo());
		Livros.add(livro);
		serializarLivro(Livros);
	}

	public void removerLivro(Livro livro) throws IOException {
		for (Livro livro_interno : Livros) {
			if (livro_interno.getTitulo().equalsIgnoreCase(livro.getTitulo())
					&& livro_interno.getAutor().equalsIgnoreCase(
							livro.getAutor())) {
				Livros.remove(livro_interno);
				break;
			}
		}
		serializarLivro(Livros);
	}

	public Livro consultarLivro(String titulo) throws ErroBanco {
		for (Livro livro : Livros) {
			if (livro.getTitulo().equalsIgnoreCase(titulo)) {
				return livro;
			}
		}
		throw new ErroBanco("Livro inexistente.");
	}

	public String consultarLivros() {
		if (Livros.isEmpty())
			return null;
		String result = "Livros \n";
		for (Livro livro : Livros) {
			result += livro.toString();
		}
		return result;
	}
}