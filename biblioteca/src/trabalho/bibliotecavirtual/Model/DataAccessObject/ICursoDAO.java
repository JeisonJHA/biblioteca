package trabalho.bibliotecavirtual.Model.DataAccessObject;

import trabalho.bibliotecavirtual.Model.ValueObject.Curso;

// Interface that all CursoDAOs must support
public interface ICursoDAO {
	public void inserirCurso(String nome) throws Exception;
	public void removerCurso(String nome) throws Exception;
	public Curso consultarCurso(String nome) throws Exception;
	public String consultarCursos() throws Exception;
	public void inserirCurso(Curso curso) throws Exception;
}