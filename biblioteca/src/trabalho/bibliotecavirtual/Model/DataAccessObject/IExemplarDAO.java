package trabalho.bibliotecavirtual.Model.DataAccessObject;

import trabalho.bibliotecavirtual.Model.ValueObject.Emprestimo;
import trabalho.bibliotecavirtual.Model.ValueObject.Exemplar;

// Interface that all ExemplarDAOs must support
public interface IExemplarDAO {
	public void inserirExemplar(String titulo, Exemplar exemplar) throws Exception;
	public void removerExemplar(String titulo, Exemplar exemplar) throws Exception;
	public Exemplar consultarExemplar(int codigo) throws Exception;
	public String consultarExemplares() throws Exception;
	public String consultarExemplares(Emprestimo emprestimo) throws Exception;
	public int consultarQuantidadeExemplar(Emprestimo emprestimo) throws Exception;
}