package trabalho.bibliotecavirtual.Model.DataAccessObject;

// PostgreExemplarDAO implementation of the 
// ExemplarDAO interface. This class can contain all
// Postgre specific code and SQL statements. 
// The client is thus shielded from knowing 
// these implementation details.

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import trabalho.bibliotecavirtual.Model.BusinessObject.ErroBanco;
import trabalho.bibliotecavirtual.Model.ValueObject.Emprestimo;
import trabalho.bibliotecavirtual.Model.ValueObject.Exemplar;

import com.sun.rowset.CachedRowSetImpl;

public class PostgreExemplarDAO extends DBBase implements IExemplarDAO {

	public PostgreExemplarDAO() {
		// initialization
	}

	public void inserirExemplar(String titulo, Exemplar exemplar)
			throws ErroBanco {
		List<Parametro> parametros = new ArrayList<Parametro>();
		parametros
				.add(new Parametro(exemplar.getEdicao(), TipoParametro.Texto));
		parametros.add(new Parametro(exemplar.getLocalizacao(),
				TipoParametro.Texto));
		parametros.add(new Parametro(exemplar.getQuantidade(),
				TipoParametro.Numero));
		new PostgreLivroDAO().consultarLivro(titulo);
		executaIntervencao(
				"INSERT INTO Exemplar(edicao, localizacao, quantidade) VALUES(?,?,?)",
				parametros);
	}

	public void removerExemplar(String titulo, Exemplar exemplar)
			throws ErroBanco {
		List<Parametro> parametros = new ArrayList<Parametro>();
		parametros.add(new Parametro(titulo, TipoParametro.Texto));
		executaIntervencao("DELETE FROM Exemplar WHERE", parametros);
	}

	public void emprestarExemplar(Emprestimo emprestimo) throws ErroBanco {
		List<Parametro> parametros = new ArrayList<Parametro>();
		parametros.add(new Parametro(emprestimo.getTitulo(),
				TipoParametro.Texto));
		executaIntervencao("INSERT INTO Emprestimo() VALUES()", parametros);
	}

	public void devolverExemplar(Emprestimo emprestimo) throws ErroBanco {
		List<Parametro> parametros = new ArrayList<Parametro>();
		parametros.add(new Parametro(emprestimo.getTitulo(),
				TipoParametro.Texto));
		executaIntervencao("UPDATE Emprestimo SET dataentrega = ? WHERE",
				parametros);
	}

	public Exemplar consultarExemplar(int codigo) throws ErroBanco {
		List<Parametro> parametros = new ArrayList<Parametro>();
		parametros.add(new Parametro(codigo, TipoParametro.Numero));
		executaConsulta(
				"SELECT localizacao, edicao, quantidade FROM Exemplar WHERE id_exemplar = ?",
				parametros);
		return null;
	}

	public String consultarExemplares() throws ErroBanco {
		CachedRowSetImpl resultado = null;
		try {
			resultado = executaConsulta(
					"SELECT l.titulo, e.edicao, e.localizacao, e.quantidade FROM livro l, exemplar e WHERE l.id_livro = e.id_livro",
					null);
			String result = "";
			while (resultado.next())
				result += "Livro: " + resultado.getString(1) + "; Edi��o: "
						+ resultado.getString(2) + "; Localiza��o: "
						+ resultado.getString(3) + "; Quantidade: "
						+ resultado.getInt(4) + "\n";
			return result;
		} catch (SQLException e) {
			throw new ErroBanco("Erro na leitura de dados. Mensagem original: "
					+ e.getMessage());
		}
	}

	public String consultarExemplares(Emprestimo emprestimo) throws ErroBanco {
		List<Parametro> parametros = new ArrayList<Parametro>();
		executaConsulta("", parametros);
		return null;
	}

	public int consultarQuantidadeExemplar(Emprestimo emprestimo)
			throws ErroBanco {
		CachedRowSetImpl resultado = null;
		try {
			List<Parametro> parametros = new ArrayList<Parametro>();
			parametros.add(new Parametro(emprestimo.getTitulo(),
					TipoParametro.Texto));
			parametros.add(new Parametro(emprestimo.getAutor(),
					TipoParametro.Texto));
			resultado = executaConsulta("SELECT quantidade FROM Exemplar "
					+ "JOIN Livro l ON e.id_livro = l.id_livro"
					+ "where l.titulo = ? and l.autor = ?", parametros);
			return resultado.getInt(1);
		} catch (SQLException e) {
			throw new ErroBanco("Erro na leitura de dados. Mensagem original: "
					+ e.getMessage());
		}
	}
}