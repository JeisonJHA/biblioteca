package trabalho.bibliotecavirtual.Model.DataAccessObject;

import trabalho.bibliotecavirtual.Model.ValueObject.Livro;

// Interface that all LivroDAOs must support
public interface ILivroDAO {
	public void inserirLivro(Livro livro) throws Exception;
	public void removerLivro(Livro livro) throws Exception;
	public Livro consultarLivro(String titulo) throws Exception;
	public String consultarLivros() throws Exception;
}