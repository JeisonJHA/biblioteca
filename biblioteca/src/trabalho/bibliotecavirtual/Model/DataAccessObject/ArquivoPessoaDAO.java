package trabalho.bibliotecavirtual.Model.DataAccessObject;

// ArquivoPessoaDAO implementation of the 
// PessoaDAO interface. This class can contain all
// Arquivo specific code and SQL statements. 
// The client is thus shielded from knowing 
// these implementation details.

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import trabalho.bibliotecavirtual.Model.BusinessObject.FuncoesBasicas;
import trabalho.bibliotecavirtual.Model.ValueObject.Aluno;
import trabalho.bibliotecavirtual.Model.ValueObject.Pessoa;
import trabalho.bibliotecavirtual.Model.ValueObject.Professor;

public class ArquivoPessoaDAO extends ArquivoBase implements IPessoaDAO {
	private static List<Pessoa> Clientes;

	public ArquivoPessoaDAO() {
		Clientes = new ArrayList<Pessoa>();
		carregarDados();
	}

	private void carregarDados() {
		deserializarCliente();
	}

	private static int pegarProximoCodigo() {
		if (Clientes.isEmpty())
			return 1;

		return Clientes.get(Clientes.lastIndexOf(Clientes)).getCodigo();
	}

	@SuppressWarnings("unchecked")
	private void deserializarCliente() {
		try {
			Clientes = (List<Pessoa>) deserializarObjeto("Cliente");
		} catch (IOException e) {
		}
	}

	private void serializarCliente(List<Pessoa> cliente) throws IOException {
		serializarObjeto(cliente, "Cliente");
	}

	public void inserirCliente(Pessoa pessoa) throws IOException {
		pessoa.setCodigo(pegarProximoCodigo());
		Clientes.add(pessoa);
		serializarCliente(Clientes);
	}

	public void removerCliente(Pessoa pessoa) throws IOException {
		Clientes.remove(pessoa);
		serializarCliente(Clientes);
	}

	public Pessoa consultarCliente(String nome) {
		for (Pessoa pessoa : Clientes) {
			if (pessoa.getNome().equalsIgnoreCase(nome)) {
				return pessoa;
			}
		}
		return null;
	}

	public Pessoa consultarCliente(int matricula) {
		for (Pessoa pessoa : Clientes) {
			if (pessoa.getMatricula() == matricula) {
				return pessoa;
			}
		}
		return null;
	}

	public String consultarClientes(int tipoCliente) {
		if (Clientes.isEmpty())
			return null;
		String result = "";
		for (Pessoa pessoa : Clientes) {
			if (pessoa instanceof Aluno && tipoCliente == FuncoesBasicas.ALUNO)
				result += pessoa.toString() + "\n";
			else if (pessoa instanceof Professor
					&& tipoCliente == FuncoesBasicas.PROFESSOR) {
				result += pessoa.toString() + "\n";
			}
		}
		if (result == "")
			return null;
		return getDescricaoCliente(tipoCliente) + " \n" + result;
	}

	private String getDescricaoCliente(int tipoCliente) {
		if (tipoCliente == FuncoesBasicas.ALUNO) {
			return "Alunos";
		} else if (tipoCliente == FuncoesBasicas.PROFESSOR) {
			return "Professores";
		}
		return "Clientes";
	}

	public void excluirCliente(String nome) throws Exception {
		for (Pessoa pessoa : Clientes) {
			if (pessoa.getNome().equalsIgnoreCase(nome)) {
				Clientes.remove(pessoa);
				break;
			}
		}
		serializarCliente(Clientes);
	}
}