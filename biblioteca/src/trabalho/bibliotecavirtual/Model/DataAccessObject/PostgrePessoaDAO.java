package trabalho.bibliotecavirtual.Model.DataAccessObject;

// PostgrePessoaDAO implementation of the 
// PessoaDAO interface. This class can contain all
// Postgre specific code and SQL statements. 
// The client is thus shielded from knowing 
// these implementation details.

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import trabalho.bibliotecavirtual.Model.BusinessObject.ErroBanco;
import trabalho.bibliotecavirtual.Model.ValueObject.Aluno;
import trabalho.bibliotecavirtual.Model.ValueObject.Curso;
import trabalho.bibliotecavirtual.Model.ValueObject.Pessoa;
import trabalho.bibliotecavirtual.Model.ValueObject.Professor;

import com.sun.rowset.CachedRowSetImpl;

public class PostgrePessoaDAO extends DBBase implements IPessoaDAO {
	final static int ALUNO = 1;
	final static int PROFESSOR = 2;

	public PostgrePessoaDAO() {
		// initialization
	}

	private int getTipoPessoa(Pessoa pessoa) throws ErroBanco {
		if (pessoa instanceof Aluno) {
			return ALUNO;
		} else if (pessoa instanceof Professor) {
			return PROFESSOR;
		}
		throw new ErroBanco("Tipo objeto pessoa inv�lido.");
	}

	private Pessoa getObjetoPessoa(int tipoPessoa) {
		if (tipoPessoa == ALUNO) {
			return new Aluno();
		} else if (tipoPessoa == PROFESSOR) {
			return new Professor();
		}
		return null;
	}

	public void inserirCliente(Pessoa pessoa) throws ErroBanco {
		int tipoPessoa = getTipoPessoa(pessoa);
		List<Parametro> parametros = new ArrayList<Parametro>();
		parametros.add(new Parametro(pessoa.getNome(), TipoParametro.Texto));
		parametros.add(new Parametro(pessoa.getMatricula(),
				TipoParametro.Numero));
		parametros.add(new Parametro(tipoPessoa, TipoParametro.Numero));

		executaIntervencao(
				"INSERT INTO Pessoa(nome, matricula, tipopessoa) VALUES (?,?,?)",
				parametros);
		if (tipoPessoa == ALUNO) {
			Aluno cliente = (Aluno) pessoa;
			if (cliente.getCursoMatriculado().isEmpty())
				return;
			Curso curso = new PostgreCursoDAO().consultarCurso(cliente.getCursoMatriculado());
			atribuirCursoCliente(curso.getCodigo(), cliente.getMatricula());
		} else if (tipoPessoa == PROFESSOR) {
			Professor cliente = (Professor) pessoa;
			if (cliente.getCursos().isEmpty()) {
				return;
			}
			for (String nomeCurso : cliente.getCursos()) {
				Curso curso = new PostgreCursoDAO().consultarCurso(nomeCurso);
				atribuirCursoCliente(curso.getCodigo(), cliente.getMatricula());
			}
		}
	}

	private void atribuirCursoCliente(int codigoCurso, int codigoCliente)
			throws ErroBanco {
		List<Parametro> parametros = new ArrayList<Parametro>();
		parametros.add(new Parametro(codigoCurso, TipoParametro.Numero));
		parametros.add(new Parametro(codigoCliente, TipoParametro.Numero));

		executaIntervencao(
				"INSERT INTO Rel_Pessoa_Curso(id_pessoa, id_curso) VALUES (?,?)",
				parametros);
	}

	public void removerCliente(Pessoa pessoa) throws ErroBanco {
		int tipoPessoa = getTipoPessoa(pessoa);
		if (tipoPessoa == ALUNO) {
			Aluno cliente = (Aluno) pessoa;
			if (!cliente.getCursoMatriculado().isEmpty()) {
				Curso curso = new PostgreCursoDAO().consultarCurso(cliente.getCursoMatriculado());
				removerCursoCliente(curso.getCodigo(), cliente.getMatricula());
			}
		} else if (tipoPessoa == PROFESSOR) {
			Professor cliente = (Professor) pessoa;
			if (cliente.getCursos().isEmpty()) {
				return;
			}
			for (String nomeCurso : cliente.getCursos()) {
				Curso curso = new PostgreCursoDAO().consultarCurso(nomeCurso);
				removerCursoCliente(curso.getCodigo(), cliente.getMatricula());
			}
		}
		List<Parametro> parametros = new ArrayList<Parametro>();
		parametros.add(new Parametro(pessoa.getMatricula(),
				TipoParametro.Numero));

		executaIntervencao("DELETE FROM Pessoa WHERE matricula = ?)",
				parametros);
	}

	private void removerCursoCliente(int codigoCurso, int codigoCliente)
			throws ErroBanco {
		List<Parametro> parametros = new ArrayList<Parametro>();
		parametros.add(new Parametro(codigoCurso, TipoParametro.Numero));
		parametros.add(new Parametro(codigoCliente, TipoParametro.Numero));
		executaIntervencao(
				"DELETE FROM Rel_Pessoa_Curso WHERE id_pessoa = ? and id_curso = ?",
				parametros);
	}

	public Pessoa consultarCliente(String nome) throws ErroBanco {
		CachedRowSetImpl resultado = null;
		List<Parametro> parametros = new ArrayList<Parametro>();
		parametros.add(new Parametro(nome, TipoParametro.Texto));
		resultado = executaConsulta(
				"SELECT nome, matricula, tipopessoa FROM Pessoa WHERE nome = ?",
				parametros);
		try {
			Pessoa pessoa = getObjetoPessoa(resultado.getInt(3));
			pessoa.setNome(resultado.getString(1));
			pessoa.setMatricula(resultado.getInt(2));
			return pessoa;
		} catch (SQLException e) {
			throw new ErroBanco("Erro na leitura de dados. Mensagem original: "
					+ e.getMessage());
		}
	}

	public Pessoa consultarCliente(int matricula) throws ErroBanco {
		CachedRowSetImpl resultado = null;
		List<Parametro> parametros = new ArrayList<Parametro>();
		parametros.add(new Parametro(matricula, TipoParametro.Numero));
		resultado = executaConsulta(
				"SELECT nome, matricula, tipopessoa FROM Pessoa WHERE matricula = ?",
				parametros);
		try {
			return getObjetoPessoa(resultado.getInt(3));
		} catch (SQLException e) {
			throw new ErroBanco("Erro na leitura de dados. Mensagem original: "
					+ e.getMessage());
		}
	}

	@Override
	public String consultarClientes(int tipoCliente) throws ErroBanco {
		CachedRowSetImpl resultado = null;
		try {
			List<Parametro> parametros = new ArrayList<Parametro>();
			parametros.add(new Parametro(tipoCliente, TipoParametro.Numero));
			resultado = executaConsulta(
					"SELECT id_pessoa, nome, matricula FROM Pessoa WHERE tipopessoa = ?",
					parametros);
			String result = "";
			while (resultado.next())
				result += "Nome: " + resultado.getString(2) + "; Matricula: "
						+ resultado.getInt(3) + "\n";
			return result;
		} catch (SQLException e) {
			throw new ErroBanco("Erro na leitura de dados. Mensagem original: "
					+ e.getMessage());
		}
	}

	public void excluirCliente(String nome) throws ErroBanco {
		removerCliente(consultarCliente(nome));
	}
}