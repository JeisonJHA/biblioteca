package trabalho.bibliotecavirtual.Model.DataAccessObject;

public class Parametro {
	private Object campo;
	private TipoParametro tipo;
	
	public Parametro(Object campo, TipoParametro tipo) {
		this.campo = campo;
		this.tipo = tipo;
	}
	
	public Object getCampo() {
		return campo;
	}
	public void setCampo(Object campo) {
		this.campo = campo;
	}
	public TipoParametro getTipo() {
		return tipo;
	}
	public void setTipo(TipoParametro tipo) {
		this.tipo = tipo;
	}

}
