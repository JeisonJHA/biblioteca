package trabalho.bibliotecavirtual.Model.DataAccessObject;

// ArquivoCursoDAO implementation of the 
// CursoDAO interface. This class can contain all
// Arquivo specific code and SQL statements. 
// The client is thus shielded from knowing 
// these implementation details.

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import trabalho.bibliotecavirtual.Model.ValueObject.Curso;

public class ArquivoCursoDAO extends ArquivoBase implements ICursoDAO {
	private static List<Curso> Cursos;

	public ArquivoCursoDAO() {
		Cursos = new ArrayList<Curso>();
		deserializarCurso();
	}

	@SuppressWarnings("unchecked")
	private void deserializarCurso() {
		try {
			Cursos = (List<Curso>) deserializarObjeto("Curso");
		} catch (IOException e) {
		}
	}

	private static int pegarProximoCodigo() {
		if (Cursos.isEmpty())
			return 1;

		return Cursos.get(Cursos.lastIndexOf(Cursos)).getCodigo();
	}

	private void serializarCurso(List<Curso> curso) throws IOException {
		serializarObjeto(curso, "Curso");
	}

	public void inserirCurso(String nome) throws Exception {
		Curso curso = new Curso(nome);
		curso.setCodigo(pegarProximoCodigo());
		Cursos.add(curso);
		serializarCurso(Cursos);
	}

	public void removerCurso(String nome) throws Exception {
		Curso curso = consultarCurso(nome);
		Cursos.remove(curso);
		serializarCurso(Cursos);
	}

	public Curso consultarCurso(String nome) throws Exception {
		for (Curso curso : Cursos) {
			if (curso.getNome().equalsIgnoreCase(nome)) {
				return curso;
			}
		}
		return null;
	}

	public String consultarCursos() throws Exception {
		if (Cursos.isEmpty())
			return null;
		String result = "Cursos \n";
		for (Curso curso : Cursos) {
			result += curso.toString() + "\n";
		}
		return result;
	}

	public void inserirCurso(Curso curso) throws Exception {
		curso.setCodigo(pegarProximoCodigo());
		Cursos.add(curso);
		serializarCurso(Cursos);
	}

}