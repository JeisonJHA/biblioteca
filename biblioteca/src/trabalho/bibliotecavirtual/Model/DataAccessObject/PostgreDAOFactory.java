package trabalho.bibliotecavirtual.Model.DataAccessObject;

// Cloudscape concrete DAO Factory implementation
public class PostgreDAOFactory extends DAOFactory {
	
	public ICursoDAO getCursoDAO() {
		// PostgreCursoDAO implements CursoDAO
		return new PostgreCursoDAO();
	}

	public ILivroDAO getLivroDAO() {
		// PostgreLivroDAO implements LivroDAO
		return new PostgreLivroDAO();
	}

	public IPessoaDAO getPessoaDAO() {
		// PostgrePessoaDAO implements PessoaDAO
		return new PostgrePessoaDAO();
	}

	public IExemplarDAO getExemplarDAO() {
		// PostgreExemplarDAO implements ExemplarDAO
		return new PostgreExemplarDAO();
	}

	public IEmprestimoDAO getEmprestimoDAO() {
		// PostgreEmprestimoDAO implements EmprestimoDAO
		return new PostgreEmprestimoDAO();
	}
}