package trabalho.bibliotecavirtual.Model.DataAccessObject;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import trabalho.bibliotecavirtual.Model.BusinessObject.ErroBanco;

import com.sun.rowset.CachedRowSetImpl;

public class DBBase {
	public static final String DRIVER = "org.postgresql.Driver";
	public static final String DBURL = "jdbc:postgresql://localhost:5432/bibliotecavirtual";

	// method to create Cloudscape connections
	private static Connection getConexao() throws ErroBanco {
		Connection conexao = null;
		try {
			String user = "postgres";
			String password = "postgres";
			Class.forName(DRIVER);
			conexao = DriverManager.getConnection(DBURL, user, password);
		} catch (Exception e) {
			throw new ErroBanco("Falha na conex�o." + e.getMessage());
		}
		return conexao;
	}

	private void preencheParametro(PreparedStatement st,
			List<Parametro> parametros) throws SQLException {
		if (parametros == null)
			return;

		int pos = 1;
		for (Parametro parametro : parametros) {
			switch (parametro.getTipo()) {
			case Texto:
				st.setString(pos, (String) parametro.getCampo());
				break;
			case Numero:
				st.setInt(pos, (int) parametro.getCampo());
				break;
			case Data:
				st.setDate(pos, (Date) parametro.getCampo());
				break;
			}
			pos++;
		}
	}

	public CachedRowSetImpl executaConsulta(String consulta) throws ErroBanco {
		return executaConsulta(consulta, null);
	}

	public CachedRowSetImpl executaConsulta(String consulta,
			List<Parametro> parametros) throws ErroBanco {
		Connection conexao = null;
		PreparedStatement st = null;
		ResultSet resultado = null;
		try {
			CachedRowSetImpl crs = new CachedRowSetImpl();
			conexao = getConexao();
			st = conexao.prepareStatement(consulta);
			preencheParametro(st, parametros);
			resultado = st.executeQuery();
			crs.populate(resultado);
			return crs;
		} catch (ErroBanco e) {
			throw new ErroBanco(e.getMessage());
		} catch (Exception e) {
			throw new ErroBanco("Erro na execu��o do select.");
		} finally {
			try {
				if (st != null)
					st.close();
				if (conexao != null)
					conexao.close();
			} catch (Exception e) {
			}
		}
	}

	public void executaIntervencao(String consulta, List<Parametro> parametros)
			throws ErroBanco {
		Connection conexao = null;
		PreparedStatement st = null;
		try {
			conexao = getConexao();
			st = conexao.prepareStatement(consulta);
			preencheParametro(st, parametros);
			st.execute();
		} catch (Exception e) {
			throw new ErroBanco("Erro na execu��o do select.");
		} finally {
			try {
				if (st != null)
					st.close();
				if (conexao != null)
					conexao.close();
			} catch (Exception e) {
			}
		}
	}

}
