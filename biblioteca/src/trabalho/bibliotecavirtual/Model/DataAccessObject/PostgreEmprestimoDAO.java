package trabalho.bibliotecavirtual.Model.DataAccessObject;

// PostgreEmprestimoDAO implementation of the 
// EmprestimoDAO interface. This class can contain all
// Postgre specific code and SQL statements. 
// The client is thus shielded from knowing 
// these implementation details.

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import trabalho.bibliotecavirtual.Model.BusinessObject.ErroBanco;
import trabalho.bibliotecavirtual.Model.ValueObject.Emprestimo;

import com.sun.rowset.CachedRowSetImpl;

public class PostgreEmprestimoDAO extends DBBase implements IEmprestimoDAO {

	public PostgreEmprestimoDAO() {
		// initialization
	}

	public void emprestarExemplar(Emprestimo emprestimo) throws ErroBanco {
		List<Parametro> parametros = new ArrayList<Parametro>();
		parametros.add(new Parametro(emprestimo.getTitulo(),
				TipoParametro.Texto));
		executaIntervencao("INSERT INTO Emprestimo() VALUES()", parametros);
	}

	public void devolverExemplar(Emprestimo emprestimo) throws ErroBanco {
		List<Parametro> parametros = new ArrayList<Parametro>();
		parametros.add(new Parametro(emprestimo.getTitulo(),
				TipoParametro.Texto));
		executaIntervencao("UPDATE Emprestimo SET dataentrega = ? WHERE",
				parametros);
	}

	public int consultarQuantidadeExemplar(Emprestimo emprestimo)
			throws ErroBanco {
		CachedRowSetImpl resultado = null;
		try {
			List<Parametro> parametros = new ArrayList<Parametro>();
			parametros.add(new Parametro(emprestimo.getTitulo(),
					TipoParametro.Texto));
			parametros.add(new Parametro(emprestimo.getAutor(),
					TipoParametro.Texto));
			resultado = executaConsulta("SELECT quantidade FROM Exemplar "
					+ "JOIN Livro l ON e.id_livro = l.id_livro"
					+ "where l.titulo = ? and l.autor = ?", parametros);
			return resultado.getInt(1);
		} catch (SQLException e) {
			throw new ErroBanco("Erro na leitura de dados. Mensagem original: "
					+ e.getMessage());
		}
	}

	@Override
	public Emprestimo consultarEmprestimo(Emprestimo emprestimo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String consultarEmprestimos() {
		// TODO Auto-generated method stub
		return null;
	}
}