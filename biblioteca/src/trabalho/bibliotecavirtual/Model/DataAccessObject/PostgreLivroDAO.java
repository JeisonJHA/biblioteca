package trabalho.bibliotecavirtual.Model.DataAccessObject;

// PostgreLivroDAO implementation of the 
// LivroDAO interface. This class can contain all
// Postgre specific code and SQL statements. 
// The client is thus shielded from knowing 
// these implementation details.

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import trabalho.bibliotecavirtual.Model.BusinessObject.ErroBanco;
import trabalho.bibliotecavirtual.Model.ValueObject.Livro;

import com.sun.rowset.CachedRowSetImpl;

public class PostgreLivroDAO extends DBBase implements ILivroDAO {

	public PostgreLivroDAO() {
		// initialization
	}

	public void inserirLivro(Livro livro) throws ErroBanco {
		List<Parametro> parametros = new ArrayList<Parametro>();
		parametros.add(new Parametro(livro.getTitulo(), TipoParametro.Texto));
		parametros.add(new Parametro(livro.getAutor(), TipoParametro.Texto));
		executaIntervencao("INSERT INTO Livro(titulo, autor) VALUES (?,?)",
				parametros);
	}

	public void removerLivro(Livro livro) throws ErroBanco {
		List<Parametro> parametros = new ArrayList<Parametro>();
		parametros.add(new Parametro(livro.getCodigo(), TipoParametro.Numero));
		executaIntervencao("DELETE FROM Livro WHERE id_livro = ?", parametros);
	}

	public Livro consultarLivro(String titulo) throws ErroBanco {
		CachedRowSetImpl resultado = null;
		List<Parametro> parametros = new ArrayList<Parametro>();
		parametros.add(new Parametro(titulo, TipoParametro.Texto));
		resultado = executaConsulta(
				"SELECT titulo, autor FROM Livro WHERE titulo = ?", parametros);
		try {
			return new Livro(resultado.getString(1), resultado.getString(2));
		} catch (Exception e) {
			throw new ErroBanco("Erro na leitura de dados. Mensagem original: "
					+ e.getMessage());
		}
	}

	public String consultarLivros() throws ErroBanco {
		CachedRowSetImpl resultado = null;
		try {
			resultado = executaConsulta("SELECT titulo, autor FROM Livro", null);
			String result = "";
			while (resultado.next())
				result += "T�tulo: " + resultado.getString(1) + "; Autor: "
						+ resultado.getString(2) + "\n";
			return result;
		} catch (SQLException e) {
			throw new ErroBanco("Erro na leitura de dados. Mensagem original: "
					+ e.getMessage());
		}
	}
}