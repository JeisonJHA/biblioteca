package trabalho.bibliotecavirtual.Model.DataAccessObject;

// ArquivoEmprestimoDAO implementation of the 
// EmprestimoDAO interface. This class can contain all
// Arquivo specific code and SQL statements. 
// The client is thus shielded from knowing 
// these implementation details.

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import trabalho.bibliotecavirtual.Model.BusinessObject.ErroBanco;
import trabalho.bibliotecavirtual.Model.ValueObject.Emprestimo;
import trabalho.bibliotecavirtual.Model.ValueObject.Exemplar;
import trabalho.bibliotecavirtual.Model.ValueObject.Livro;

public class ArquivoEmprestimoDAO extends ArquivoBase implements IEmprestimoDAO {
	private static List<Emprestimo> Emprestimos;

	public ArquivoEmprestimoDAO() {
		Emprestimos = new ArrayList<Emprestimo>();
		deserializarEmprestimos();
	}

	@SuppressWarnings("unchecked")
	private void deserializarEmprestimos() {
		try {
			Emprestimos = (List<Emprestimo>) deserializarObjeto("Emprestimos");
		} catch (IOException e) {
		}
	}

	private void serializarEmprestimo(List<Emprestimo> emprestimo)
			throws IOException {
		serializarObjeto(emprestimo, "Emprestimos");
	}

	private static int pegarProximoCodigo() {
		if (Emprestimos.isEmpty())
			return 1;

		return Emprestimos.get(Emprestimos.lastIndexOf(Emprestimos))
				.getCodigo();
	}

	public void emprestarExemplar(Emprestimo emprestimo) throws Exception {
		if (consultarQuantidadeExemplar(emprestimo) <= 0) {
			throw new ErroBanco("N�o possui exemplar.");
		}
		emprestimo.setCodigo(pegarProximoCodigo());
		Emprestimos.add(emprestimo);
		serializarEmprestimo(Emprestimos);
	}

	public Emprestimo consultarEmprestimo(Emprestimo emprestimo) {
		for (Emprestimo emprest : Emprestimos) {
			if (!emprest.getAutor().equalsIgnoreCase(emprestimo.getAutor()))
				continue;
			if (!emprest.getEdicao().equalsIgnoreCase(emprestimo.getEdicao()))
				continue;
			if (!emprest.getTitulo().equalsIgnoreCase(emprestimo.getTitulo()))
				continue;
			return emprest;
		}
		return null;
	}

	public String consultarEmprestimos() {
		String result = "";
		for (Emprestimo emprest : Emprestimos) {
			result += emprest.toString();
		}
		return result;
	}

	public void devolverExemplar(Emprestimo emprestimo) throws Exception {
		Emprestimo emp = consultarEmprestimo(emprestimo);
		if (emp == null)
			throw new ErroBanco("Emprestimo inexistente.");
		emp.setDataEntrega(new Date(System.currentTimeMillis()));		
		serializarEmprestimo(Emprestimos);
	}

	public int consultarQuantidadeExemplar(Emprestimo emprestimo)
			throws ErroBanco {
		int totalEmprestado = 0;
		int totalExemplares = 0;
		Livro livro = new ArquivoLivroDAO().consultarLivro(emprestimo
				.getTitulo());
		List<Exemplar> Exemplares = livro.getExemplares();
		for (Exemplar exemplar : Exemplares) {
			if (exemplar.getEdicao().equalsIgnoreCase(emprestimo.getEdicao())){
				totalExemplares = exemplar.getQuantidade();				
			}
		}
		for (Emprestimo emprest : Emprestimos) {
			if (!emprest.getAutor().equalsIgnoreCase(emprestimo.getAutor()))
				continue;
			if (!emprest.getEdicao().equalsIgnoreCase(emprestimo.getEdicao()))
				continue;
			if (!emprest.getTitulo().equalsIgnoreCase(emprestimo.getTitulo()))
				continue;
			if (!(emprest.getDataEntrega() == null))
				continue;
			totalEmprestado ++;
		}
		return totalExemplares - totalEmprestado;
	}
}