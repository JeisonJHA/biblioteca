package trabalho.bibliotecavirtual.Model.DataAccessObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public abstract class ArquivoBase {
	
	protected static void serializarObjeto(Object objeto, String arquivo) throws IOException {
    OutputStream escritorbyte = null;
    ObjectOutputStream escritorObjeto = null;
    try {
      escritorbyte = new FileOutputStream("dados/" + arquivo + ".bin");
      escritorObjeto = new ObjectOutputStream(escritorbyte);

      escritorObjeto.writeObject(objeto);
      escritorObjeto.flush();
    } catch (IOException e) {
      throw new IOException("Erro arquivo n�o encontrado");
    } finally {
      try {
        if (escritorObjeto != null)
          escritorObjeto.close();
        if (escritorbyte != null)
          escritorbyte.close();
      } catch (Exception e) {
      }
    }
  }
  
  protected static Object deserializarObjeto(String arquivo) throws IOException {
    InputStream leitorByte = null;
    ObjectInputStream leitorObjeto = null;
    Object retorno = null;
    try {
      leitorByte = new FileInputStream("dados/" + arquivo + ".bin");
      leitorObjeto = new ObjectInputStream(leitorByte);
      retorno = leitorObjeto.readObject();
    } catch (FileNotFoundException e) {
      throw new IOException("Erro arquivo n�o encontrado");
    } catch (IOException e) {
      throw new IOException("Erro arquivo n�o encontrado");
    } catch (ClassNotFoundException e) {
      throw new IOException("Erro arquivo n�o encontrado");
    } finally {
      try {
        if (leitorByte != null)
          leitorByte = null;
        if (leitorObjeto != null)
          leitorObjeto.close();
      } catch (Exception e) {
      }

    }
    return retorno;
  }
}
