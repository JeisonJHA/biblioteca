package trabalho.bibliotecavirtual.Model.DataAccessObject;

// ArquivoExemplarDAO implementation of the 
// ExemplarDAO interface. This class can contain all
// Arquivo specific code and SQL statements. 
// The client is thus shielded from knowing 
// these implementation details.

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import trabalho.bibliotecavirtual.Model.BusinessObject.ErroBanco;
import trabalho.bibliotecavirtual.Model.ValueObject.Emprestimo;
import trabalho.bibliotecavirtual.Model.ValueObject.Exemplar;
import trabalho.bibliotecavirtual.Model.ValueObject.Livro;

public class ArquivoExemplarDAO extends ArquivoBase implements IExemplarDAO {
	private static List<Livro> Livros;

	public ArquivoExemplarDAO() {
		Livros = new ArrayList<Livro>();
		deserializarLivro();
	}

	private static int pegarProximoCodigo() {
		int novoCodigo = 0;
		for (Livro livro : Livros) {
			for (Exemplar exemplar : livro.getExemplares()) {
				if (exemplar.getCodigo() > novoCodigo){
					novoCodigo = exemplar.getCodigo();
				}
			}			
		}
		return novoCodigo++;
	}

	@SuppressWarnings("unchecked")
	private void deserializarLivro() {
		try {
			Livros = (List<Livro>) deserializarObjeto("Livro");
		} catch (IOException e) {}
	}

	private void serializarLivro(List<Livro> livros) throws IOException {
		serializarObjeto(livros, "Livro");
	}

	public void inserirExemplar(String titulo, Exemplar exemplar) throws Exception {
		Livro livro = consultarLivro(titulo);
		exemplar.setCodigo(pegarProximoCodigo());
		livro.adicionaExemplar(exemplar);
		serializarLivro(Livros);
	}

	public Livro consultarLivro(String titulo) throws ErroBanco {
		for (Livro livro : Livros) {
			if (livro.getTitulo().equalsIgnoreCase(titulo)) {
				return livro;
			}
		}
		throw new ErroBanco("Livro inexistente.");
	}

	public void removerExemplar(String titulo, Exemplar exemplar)
			throws Exception {
		Livro livro = consultarLivro(titulo);
		exemplar = consultarExemplar(exemplar.getCodigo());
		livro.removeExemplar(exemplar);
		serializarLivro(Livros);
	}

	public Exemplar consultarExemplar(int codigo) throws Exception {
		List<Exemplar> exemplares;
		for (Livro livro : Livros) {
			exemplares = livro.getExemplares();
			for (Exemplar exemplar : exemplares) {
				if (exemplar.getCodigo() == codigo) {
					return exemplar;
				}
			}
		}
		return null;
	}

	public String consultarExemplares() throws Exception {
		String result = "";
		for (Livro livro : Livros) {
			List<Exemplar> Exemplares = livro.getExemplares();
			for (Exemplar exemplar : Exemplares) {
				result += exemplar.toString();
			}
		}
		return result;
	}

	public String consultarExemplares(Emprestimo emprestimo) throws Exception {
		Livro livro = consultarLivro(emprestimo.getTitulo());
		List<Exemplar> Exemplares = livro.getExemplares();
		for (Exemplar exemplar : Exemplares) {
			if (exemplar.getEdicao().equalsIgnoreCase(emprestimo.getEdicao()))
				return exemplar.toString();
		}
		return null;
	}

	public int consultarQuantidadeExemplar(Emprestimo emprestimo) throws Exception {
		Livro livro = consultarLivro(emprestimo.getTitulo());
		List<Exemplar> Exemplares = livro.getExemplares();
		for (Exemplar exemplar : Exemplares) {
			if (exemplar.getEdicao().equalsIgnoreCase(emprestimo.getEdicao()))
				return exemplar.getQuantidade();
		}
		return 0;
	}
}