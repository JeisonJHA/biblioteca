package trabalho.bibliotecavirtual.Model.BusinessObject;

import trabalho.bibliotecavirtual.Model.DataAccessObject.DAOFactory;
import trabalho.bibliotecavirtual.Model.DataAccessObject.ICursoDAO;
import trabalho.bibliotecavirtual.Model.ValueObject.Curso;

public class RegrasCurso {
	private ICursoDAO banco = null;

	public RegrasCurso() {
		banco = DAOFactory.getDAOFactory().getCursoDAO();
	}

	public void cadastrarCurso(String nome) throws Exception {
		if (banco.consultarCurso(nome) != null)
			throw new Exception("Curso j� existe.");

		banco.inserirCurso(nome);
	}

	public void removerCurso(String nome) throws Exception {
		if (banco.consultarCurso(nome) == null)
			throw new Exception("Curso n�o existe.");
		banco.removerCurso(nome);
	}

	public void validarCurso(String curso) throws Exception {
		Curso entCurso = banco.consultarCurso(curso);
		if ((entCurso == null) || (curso.equalsIgnoreCase("")))
			throw new CancelaOperacao("Curso n�o cadastrado.");
	}

	public String consultarCursos() throws Exception {
		return banco.consultarCursos();
	}

}