package trabalho.bibliotecavirtual.Model.BusinessObject;

public class ErroBanco extends Exception {
	private static final long serialVersionUID = -7025928274954296606L;
	private String msg;

	public ErroBanco(String msg) {
		super(msg);
		this.msg = msg;
	}

	public String getMessage() {
		return this.msg;
	}
}
