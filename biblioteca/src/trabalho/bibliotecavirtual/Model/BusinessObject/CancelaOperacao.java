package trabalho.bibliotecavirtual.Model.BusinessObject;

public class CancelaOperacao extends Exception {
	private static final long serialVersionUID = 5925147705523727448L;
	private String msg = "Opera��o cancelada.";

	public CancelaOperacao() {}
	
	public CancelaOperacao(String msg) {
		this.msg = msg;
	}
	
	public String getMessage() {
		return this.msg;
	}

}
