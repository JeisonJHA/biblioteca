package trabalho.bibliotecavirtual.Model.BusinessObject;

import java.util.List;

import trabalho.bibliotecavirtual.Model.DataAccessObject.DAOFactory;
import trabalho.bibliotecavirtual.Model.DataAccessObject.IPessoaDAO;
import trabalho.bibliotecavirtual.Model.ValueObject.Aluno;
import trabalho.bibliotecavirtual.Model.ValueObject.Pessoa;
import trabalho.bibliotecavirtual.Model.ValueObject.Professor;

public class RegrasCliente {
	private IPessoaDAO banco = null;

	public RegrasCliente() {
		banco = DAOFactory.getDAOFactory().getPessoaDAO();
	}

	public void excluirCliente() throws Exception {
		int matricula = FuncoesBasicas
				.informarCampoInt("Digite a matricula da pessoa a ser excluida");
		Pessoa pessoa = banco.consultarCliente(matricula);
		banco.removerCliente(pessoa);
	}

	public void inserirCliente(Pessoa pessoa) throws Exception {
		if (pessoa instanceof Aluno) {
			criaAluno((Aluno) pessoa);
			return;
		} else if (pessoa instanceof Professor) {
			criaProfessor((Professor) pessoa);
			return;
		}

		throw new Exception("Erro na defini��o de cliente.");
	}

	private void criaAluno(Aluno aluno) throws Exception {
		String cursoMatriculado = "";
		cursoMatriculado = aluno.getCursoMatriculado();
		testaCursoValidoAluno(cursoMatriculado);
		banco.inserirCliente(aluno);
	}

	private void testaCursoValidoAluno(String cursoMatriculado)
			throws Exception {
		RegrasCurso regra = new RegrasCurso();
		regra.validarCurso(cursoMatriculado);
	}

	private void criaProfessor(Professor professor) throws Exception {
		RegrasCurso regra = new RegrasCurso();
		for (String cursoLecionado : professor.getCursos()) {
			regra.validarCurso(cursoLecionado);
			if (cursoLecionado == "") {
				List<String> cursos = professor.getCursos();
				cursos.remove(cursoLecionado);
			}
		}

		banco.inserirCliente(professor);
	}

	public void excluirCliente(String nome) throws Exception {
		if (banco.consultarCliente(nome) == null) {
			throw new PersistenciaInvalida("Cliente n�o existe.");
		}
		banco.excluirCliente(nome);
	}

	public String consultarClientes(int tipoCliente) throws Exception {
		return banco.consultarClientes(tipoCliente);
	}
}