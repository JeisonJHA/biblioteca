package trabalho.bibliotecavirtual.Model.BusinessObject;

import javax.swing.JOptionPane;

public class FuncoesBasicas {
	public static final int ALUNO = 1;
	public static final int PROFESSOR = 2;
	public static int TIPO;
	
	
	public static void inicializaConstante(int valor){
		TIPO = valor;
	}

	private static void validarContinuaOperacao() throws CancelaOperacao{
		if (JOptionPane.showConfirmDialog(null,
				"Deseja cancelar a opera��o?", "Sim ou n�o?",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			throw new CancelaOperacao();
		}	
	}
	
	public static int informarCampoInt(String msg) throws CancelaOperacao {
		try {
			int result = Integer.parseInt(JOptionPane.showInputDialog(msg));
			return result;
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Valor inv�lido.");
			validarContinuaOperacao();
			return informarCampoInt(msg);
		}
	}

	public static int informarCampoInt(String msg, int[] valoresvalidos) {
		try {
			int result = Integer.parseInt(JOptionPane.showInputDialog(msg));
			for (int i = 0; i < valoresvalidos.length; i++) {
				if (result == valoresvalidos[i])
					return result;
			}

			JOptionPane.showMessageDialog(null, "Valor inv�lido.");
			return informarCampoInt(msg, valoresvalidos);
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Valor inv�lido.");
			return informarCampoInt(msg, valoresvalidos);
		}
	}

	public static String informarCampoString(String msg) throws CancelaOperacao {
		String result = JOptionPane.showInputDialog(msg);
		if (result == null || result.trim().equalsIgnoreCase("")) {
			validarContinuaOperacao();
			return informarCampoString(msg);
		}
		return result;
	}
}
