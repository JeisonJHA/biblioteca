package trabalho.bibliotecavirtual.Model.BusinessObject;

public class ValidacaoExemplar extends Exception {
	private static final long serialVersionUID = 7725040217667411772L;	
	private String msg;

	public ValidacaoExemplar(String msg) {
		super(msg);
		this.msg = msg;
	}

	public String getMessage() {
		return this.msg;
	}

}
