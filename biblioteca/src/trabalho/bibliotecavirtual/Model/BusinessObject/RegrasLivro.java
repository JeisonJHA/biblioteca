package trabalho.bibliotecavirtual.Model.BusinessObject;

import trabalho.bibliotecavirtual.Model.DataAccessObject.DAOFactory;
import trabalho.bibliotecavirtual.Model.DataAccessObject.ILivroDAO;
import trabalho.bibliotecavirtual.Model.ValueObject.Livro;

public class RegrasLivro {
	private ILivroDAO banco = null;
	
	public RegrasLivro() {
		banco = DAOFactory.getDAOFactory().getLivroDAO();
	}
	
	public void inserirLivro(Livro livro) throws Exception {
		if (banco.consultarLivro(livro.getTitulo()) != null)
			throw new Exception("Livro j� existe.");
		banco.inserirLivro(livro);
	}

	public void removerLivro(Livro livro) throws Exception {
		if (banco.consultarLivro(livro.getTitulo()) == null)
			throw new Exception("Livro n�o existe.");
		banco.removerLivro(livro);
	}

	public String consultarLivros() throws Exception {
		return banco.consultarLivros();
	}	
}