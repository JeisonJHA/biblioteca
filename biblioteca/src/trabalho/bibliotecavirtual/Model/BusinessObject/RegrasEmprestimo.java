package trabalho.bibliotecavirtual.Model.BusinessObject;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import trabalho.bibliotecavirtual.Model.DataAccessObject.DAOFactory;
import trabalho.bibliotecavirtual.Model.DataAccessObject.IEmprestimoDAO;
import trabalho.bibliotecavirtual.Model.DataAccessObject.IExemplarDAO;
import trabalho.bibliotecavirtual.Model.DataAccessObject.ILivroDAO;
import trabalho.bibliotecavirtual.Model.DataAccessObject.IPessoaDAO;
import trabalho.bibliotecavirtual.Model.ValueObject.Emprestimo;
import trabalho.bibliotecavirtual.Model.ValueObject.Pessoa;

public class RegrasEmprestimo {	
	private IEmprestimoDAO banco = null;
	
	public RegrasEmprestimo() {
		banco = DAOFactory.getDAOFactory().getEmprestimoDAO();		
	}

	private IPessoaDAO getDAOPessoa() {
		return DAOFactory.getDAOFactory().getPessoaDAO();
	}	

	private ILivroDAO getDAOLivro() {
		return DAOFactory.getDAOFactory().getLivroDAO();
	}	

	private IExemplarDAO getDAOExemplar() {
		return DAOFactory.getDAOFactory().getExemplarDAO();
	}	
	
	private void validarDadosEmprestimo(Emprestimo emprestimo, Boolean emprestando) throws Exception {
		Pessoa pessoa = getDAOPessoa().consultarCliente(emprestimo.getMatricula());
		if (pessoa == null) {
			throw new ValidacaoExemplar("Pessoa n�o existe.");
		}

		if (getDAOLivro().consultarLivro(emprestimo.getTitulo()) == null) {
			throw new ValidacaoExemplar("Livro n�o existe.");
		}
		
		if (!emprestando)
			return;

		if (getDAOExemplar().consultarExemplares(emprestimo) == null) {
			throw new ValidacaoExemplar("Exemplar n�o existe.");
		}
		
		if (banco.consultarQuantidadeExemplar(emprestimo) < 1){
			throw new ValidacaoExemplar("N�o h� exemplar dispon�vel.");			
		}
	}

	public void devolverExemplar(Emprestimo emprestimo) throws Exception {
		validarDadosEmprestimo(emprestimo, false);
		banco.devolverExemplar(emprestimo);
	}

	public void emprestarExemplar(Emprestimo emprestimo) throws Exception {
		validarDadosEmprestimo(emprestimo, true);
		emprestimo.setDataInicio(new Date(System.currentTimeMillis()));
		GregorianCalendar gc = new GregorianCalendar();
		gc.add(Calendar.DATE, 10);
		emprestimo.setDataFim(new Date(gc.getTimeInMillis()));
		banco.emprestarExemplar(emprestimo);
	}
	
	public int consultarQuantidadeExemplar(Emprestimo emprestimo) throws Exception{
		return banco.consultarQuantidadeExemplar(emprestimo);		
	}
	
	public String consultarEmprestimos(){
		return banco.consultarEmprestimos();
	}
	
	public Emprestimo consultarEmprestimo(Emprestimo emprestimo) {
		return banco.consultarEmprestimo(emprestimo);
	}
}