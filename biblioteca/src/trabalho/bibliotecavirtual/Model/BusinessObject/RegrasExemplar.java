package trabalho.bibliotecavirtual.Model.BusinessObject;

import trabalho.bibliotecavirtual.Model.DataAccessObject.DAOFactory;
import trabalho.bibliotecavirtual.Model.DataAccessObject.IExemplarDAO;
import trabalho.bibliotecavirtual.Model.ValueObject.Emprestimo;
import trabalho.bibliotecavirtual.Model.ValueObject.Exemplar;

public class RegrasExemplar {
	private IExemplarDAO banco = null;
	
	public RegrasExemplar() {
		banco = DAOFactory.getDAOFactory().getExemplarDAO();
	}
	
	public void inserirExemplar(String titulo, Exemplar exemplar)
			throws Exception {
		banco.inserirExemplar(titulo, exemplar);
	}

	public void removerExemplar(String titulo, Exemplar exemplar)
			throws Exception {
		banco.removerExemplar(titulo, exemplar);
	}

	public String consultarExemplares(Emprestimo emprestimo) throws Exception {
		return banco.consultarExemplares(emprestimo);
	};
	
	public String consultarExemplares() throws Exception {
		return banco.consultarExemplares();
	};	
}