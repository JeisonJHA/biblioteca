package trabalho.bibliotecavirtual.Model.BusinessObject;

public class PersistenciaInvalida extends Exception {
	private static final long serialVersionUID = -1327903289437296338L;
	private String msg;

	public PersistenciaInvalida(String msg) {
		super(msg);
		this.msg = msg;
	}

	public String getMessage() {
		return this.msg;
	}

}